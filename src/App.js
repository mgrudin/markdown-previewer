import React from 'react';
import marked from 'marked';
import './App.css';

const editorContent = `# Welcome to my Markdown Previewer!

## This is a sub-heading...
### And here's some other cool stuff:
  
Heres some code, \`<div></div>\`, between 2 backticks.

\`\`\`
// this is multi-line code:

function anotherExample(firstLine, lastLine) {
  if (firstLine == '...' && lastLine == '...') {
    return multiLineCode;
  }
}
\`\`\`
  
You can also make text **bold**... whoa!
Or _italic_.
Or... wait for it... **_both!_**
And feel free to go crazy ~~crossing stuff out~~.

There's also [links](https://www.freecodecamp.com), and
> Block Quotes!

And if you want to get really crazy, even tables:

Wild Header | Crazy Header | Another Header?
------------ | ------------- | ------------- 
Your content can | be here, and it | can be here....
And here. | Okay. | I think we get it.

- And of course there are lists.
  - Some are bulleted.
     - With different indentation levels.
        - That look like this.


1. And there are numbererd lists too.
1. Use just 1s if you want! 
1. But the list goes on...
- Even if you use dashes or asterisks.
* And last but not least, let's not forget embedded images:

![Markdown Logo w/ Text](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)`;

// marked.setOptions({
//   breaks: true,
// });

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      textareaValue: editorContent,
      previewValue: marked(editorContent),
    };
  }

  updatePreview(value) {
    this.setState({
      textareaValue: value,
      previewValue: marked(value),
    });
  }

  render() {
    return (
      <div className="container">
        <div className="texteditor">
          <textarea
            id="editor"
            value={this.state.textareaValue}
            onChange={(e) => this.updatePreview(e.target.value)}
          ></textarea>
        </div>
        <div
          className="preview"
          id="preview"
          dangerouslySetInnerHTML={{ __html: this.state.previewValue }}
        ></div>
      </div>
    );
  }
}

export default App;
